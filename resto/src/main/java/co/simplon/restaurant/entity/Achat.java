package co.simplon.restaurant.entity;

public class Achat {

    private int id;
    private String nomAcheteur;
    private int prix;
    private int quantite;

    
    public Achat() {
    }
    public Achat(String nomAcheteur, int prix, int quantite) {
        this.nomAcheteur = nomAcheteur;
        this.prix = prix;
        this.quantite = quantite;
    }
    public Achat(int id, String nomAcheteur, int prix, int quantite) {
        this.id = id;
        this.nomAcheteur = nomAcheteur;
        this.prix = prix;
        this.quantite = quantite;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNomAcheteur() {
        return nomAcheteur;
    }
    public void setNomAcheteur(String nomAcheteur) {
        this.nomAcheteur = nomAcheteur;
    }
    public int getPrix() {
        return prix;
    }
    public void setPrix(int prix) {
        this.prix = prix;
    }
    public int getQuantite() {
        return quantite;
    }
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

}

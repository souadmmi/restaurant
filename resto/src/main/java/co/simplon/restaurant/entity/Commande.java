package co.simplon.restaurant.entity;

public class Commande {
    int id;
    String NomClient;
    int total;

    public Commande(String NomClient, int total) {
        this.NomClient = NomClient;
        this.total = total;
    }
    public Commande(int id, String NomClient, int total) {
        this.id = id;
        this.NomClient = NomClient;
        this.total = total;
    }
    public Commande() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNomClient() {
        return NomClient;
    }
    public void setNomClient(String NomClient) {
        this.NomClient = NomClient;
    }
    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }

}

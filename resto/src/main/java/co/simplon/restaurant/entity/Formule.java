package co.simplon.restaurant.entity;

public class Formule {
    private Integer id;
    private String name;
    private Integer prix;
    
    public Formule() {
    }
    public Formule(Integer id, String name, Integer prix) {
        this.id = id;
        this.name = name;
        this.prix = prix;
    }
    public Formule(String name, Integer prix) {
        this.name = name;
        this.prix = prix;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getPrix() {
        return prix;
    }
    public void setPrix(Integer prix) {
        this.prix = prix;
    }
    



}

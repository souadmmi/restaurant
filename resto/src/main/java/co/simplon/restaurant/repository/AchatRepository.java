package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Achat;


public class AchatRepository {

    public List<Achat> findAll() {
        List<Achat> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM achat");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Achat achat = new Achat(
                    result.getInt("id"),
                    result.getString("nom_acheteur"),
                    result.getInt("prix"),
                    result.getInt("quantite")
                    );
                    list.add(achat);
                
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }

    public Achat findById(int id) {
        try (Connection connection = Database.connect()) {            
            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM achat WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Achat(result.getInt("id"),
                result.getString("nom_acheteur"),
                result.getInt("prix"),
                result.getInt("quantite"));
            }
            /*System.out.println(result);*/
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return null;
    } 


    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM achat WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public boolean persist(Achat achat) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO achat (nom_acheteur, prix, quantite) VALUES (?, ?, ?)" , Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, achat.getNomAcheteur());
            stmt.setInt(2, achat.getPrix());
            stmt.setInt(3, achat.getQuantite());
            
            if(stmt.executeUpdate() == 1) {
                
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                achat.setId(keys.getInt(1));
                return true;
            }
            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(Achat achat ){
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE achat SET nom_acheteur=?, prix=?, quantite=? WHERE id=?;");
            stmt.setString(1, achat.getNomAcheteur());
            stmt.setInt(2, achat.getPrix());
            stmt.setInt(3, achat.getQuantite());
            stmt.setInt(4, achat.getId());
            
            if(stmt.executeUpdate() == 1) {
                return true;
            }
    
            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }


}

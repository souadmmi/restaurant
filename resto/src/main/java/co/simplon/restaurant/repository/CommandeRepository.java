package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Commande;

public class CommandeRepository {
    public List<Commande> findAll() {
        List<Commande> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commande");
            ResultSet result = stmt.executeQuery();
            while (result.next()){
                Commande commande = new Commande(
                result.getInt("id"),
                result.getString("nom_client"),
                result.getInt("total"));
                list.add(commande);
            }
        System.out.println("liste crée");
        } catch (SQLException e) {
            System.out.println("error from repository");
            e.printStackTrace();
        }
        return list;
    }


public Commande findById(int id) {

    try (Connection connection = Database.connect()) {
        PreparedStatement stmt = 
        connection.prepareStatement("SELECT * FROM commande WHERE id=?");
        stmt.setInt(1, id);
        ResultSet result = stmt.executeQuery();
        if (result.next()) {
            return new Commande(
                    result.getInt("id"),
                    result.getString("nom_client"),
                    result.getInt("total"));
        }
    } catch (SQLException e) {
        System.out.println("Error From Repository");
        e.printStackTrace();
    }

    return null;
}

public boolean persist(Commande commande) {
    try (Connection connection = Database.connect()) {
        PreparedStatement stmt = connection.prepareStatement("INSERT INTO commande (nom_client,total) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, commande.getNomClient());
        stmt.setInt(2, commande.getTotal());

        if(stmt.executeUpdate() == 1) {
            ResultSet keys = stmt.getGeneratedKeys();
            keys.next();
            commande.setId(keys.getInt(1));
            return true;
        }


    } catch (SQLException e) {
        System.out.println("Error from repository");
        e.printStackTrace();
    }
    return false;
}


public boolean update(Commande commande){

    try(Connection connection = Database.connect()) {
    PreparedStatement stmt = connection.prepareStatement("UPDATE commande SET nom_client=?, total=? WHERE id=?;");
        stmt.setString(1, commande.getNomClient());
        stmt.setInt(2, commande.getTotal());
        stmt.setInt(3, commande.getId());
        if(stmt.executeUpdate() == 1) {
            return true;
        }
    } catch (SQLException e) {
        System.out.println("Error from repository");
        e.printStackTrace();
    }
        return false;
    }

    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM commande WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

}

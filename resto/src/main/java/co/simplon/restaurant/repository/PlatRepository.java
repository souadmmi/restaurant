package co.simplon.restaurant.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.restaurant.entity.Plat;

public class PlatRepository {

    public List<Plat> findAll() {

        List<Plat> list = new ArrayList<>();
    
        try (Connection connection = Database.connect()) {

            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM plat");

            ResultSet result = stmt.executeQuery();

            while (result.next()){
                Plat plat = new Plat(

                result.getInt("id"),
                result.getString("name"),
                result.getInt("prix"));

                list.add(plat);


            }

        System.out.println("liste crée");
        } catch (SQLException e) {
            System.out.println("error from repository");
            e.printStackTrace();
        }

        return list;

    }

    public Plat findById(int id) {
        
        try (Connection connection = Database.connect()) {
            
            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM plat WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Plat(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
            }

        } catch (SQLException e) {
        
            e.printStackTrace();
        }

        return null;
    }


    public Boolean delete(int id){


        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = 
            connection.prepareStatement("DELETE FROM plat WHERE id=?");
            stmt.setInt(1, id);
            
            int lignesdelete = stmt.executeUpdate();

            if(lignesdelete==1){
                return true;
            }else{return false;}


        } catch (SQLException e) {
            e.printStackTrace();

            return false;
        }
    }


    public Boolean persist(Plat plat){


        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("INSERT INTO plat (name,prix) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS );
            
            stmt.setString(1,plat.getName());
            stmt.setInt(2,plat.getPrix());

            int ligneajoutee = stmt.executeUpdate();

            if (ligneajoutee==1)
            { 
            
            ResultSet generatedKeys = stmt.getGeneratedKeys(); 
            generatedKeys.next();
            int idGenere = generatedKeys.getInt(1);
            System.out.println("ID généré pour le nouveau plat : " + idGenere);
                    
            
            

                return true;} 
            
            
            else{return false;}

        } catch (SQLException e) {
            
            e.printStackTrace();
        return false;}
    }


    public Boolean update(Plat plat){

        try (Connection connection = Database.connect()) {
        
        PreparedStatement stmt = connection.prepareStatement("UPDATE plat SET name=?,prix=? WHERE Id = ?");
        stmt.setString(1, plat.getName());
        stmt.setInt(2, plat.getPrix());
        stmt.setInt(3, plat.getId());

        int changed = stmt.executeUpdate();

        if (changed==1){
            System.out.println("success");
            return true;
        }else{
            System.out.println("raté");
            return false;}

        
        
        
        } catch (SQLException e) {
            e.printStackTrace();
            
        }

        return false;
}



}

package co.simplon.restaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.restaurant.entity.Achat;

public class AchatRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void findAllShouldReturnAListOfAchat() {
        AchatRepository repo = new AchatRepository();
        List<Achat> result = repo.findAll();

        assertEquals(10, result.size());
        assertEquals("Kevin", result.get(0).getNomAcheteur());
        assertEquals(30, result.get(0).getPrix());
        assertEquals(10, result.get(0).getQuantite());
        assertEquals(1, result.get(0).getId());

        assertNotNull(result.get(0).getNomAcheteur());
        assertNotNull(result.get(0).getPrix());
        assertNotNull(result.get(0).getQuantite());
        assertNotNull(result.get(0).getId());
    }

    
    @Test
    void findByIdWithResult() {
        AchatRepository repo = new AchatRepository();
        Achat result = repo.findById(1);

        assertEquals("Kevin", result.getNomAcheteur());
        assertEquals(30, result.getPrix());
        assertEquals(10, result.getQuantite());
        assertEquals(1, result.getId());
    }

    @Test
    void deleteSuccess() {
        AchatRepository repo = new AchatRepository();

        boolean result = repo.delete(1);
        assertTrue(result);

        assertEquals(9, repo.findAll().size());
    }
    
    @Test
    void persistSuccess() {
        AchatRepository repo = new AchatRepository();
        Achat mimi = new Achat ("Souad", 10, 10);
        boolean result = repo.persist(mimi);
        assertTrue(result);

        assertEquals(11, repo.findAll().size());
    }

    @Test
    void updateSuccess() {
        AchatRepository repo = new AchatRepository();
        Achat achat = new Achat (1, "Souad", 10, 60);
        assertTrue(repo.update(achat));
        
        Achat updated = repo.findById(1);
        assertEquals(60, updated.getQuantite());
    }
    

}

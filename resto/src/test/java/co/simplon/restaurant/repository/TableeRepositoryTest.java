package co.simplon.restaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.restaurant.entity.Tablee;

public class TableeRepositoryTest {
    
    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void findAllShouldReturnAListOfTablees() {
        TableeRepository repo = new TableeRepository();
        List<Tablee> result = repo.findAll();

        assertEquals(10, result.size());
        assertEquals("Jean Dupont", result.get(0).getNomClient());
        assertEquals("2024-03-10T19:00", result.get(0).getDate().toString());
        assertEquals(4, result.get(0).getChaise());
        assertEquals(1, result.get(0).getId());



        assertNotNull(result.get(0).getDate());
        assertNotNull(result.get(0).getChaise());
        assertNotNull(result.get(0).getNomClient());
        assertNotNull(result.get(0).getId());
    }

    @Test
    void findByIdWithResult() {
        TableeRepository repo = new TableeRepository();
        Tablee result = repo.findById(1);

        assertEquals(1, result.getId());
        assertEquals(4, result.getChaise());
        assertEquals("2024-03-10T19:00", result.getDate().toString());
        assertEquals( "Jean Dupont", result.getNomClient());
    }

    
    
        @Test
        void deleteSuccess() {
            TableeRepository repo = new TableeRepository();
            boolean result = repo.delete(1);
            assertTrue(result);    
            assertEquals(9, repo.findAll().size());
        }
    
        @Test
        void persistSuccess() {
            TableeRepository repo = new TableeRepository();
            Tablee mimi = new Tablee (LocalDateTime.of(2022, 02, 20, 10, 0, 0), "Souad", 5 );
            boolean result = repo.persist(mimi);
            assertTrue(result);
    
            assertEquals(11, repo.findAll().size());
        }

        @Test
    void updateSuccess() {
        TableeRepository repo = new TableeRepository();
        Tablee tablee = new Tablee(1, LocalDateTime.of(2022, 10, 10, 10, 0, 0), "Jean Dupont", 5);
        assertTrue(repo.update(tablee));
        
        Tablee updated = repo.findById(1);
        assertEquals(5, updated.getChaise());
    }
    
}







    